import {FETCH_FASHION_FEED_PENDING, FETCH_FASHION_FEED_FULFILLED, FETCH_FASHION_FEED_REJECTED } from './../actions/actionTypes'

export const initialState = {
  feedItems: [],
  isFetching: false
};

export default function fashionReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_FASHION_FEED_PENDING:
      return {
        ...state,
        isFetching: true
      }
    case FETCH_FASHION_FEED_FULFILLED:
      return {
        ...state,
        isFetching: false,
        feedItems: state.feedItems.concat(action.payload)
      }
    case FETCH_FASHION_FEED_REJECTED:
      return {
        ...state,
        isFetching: false,
      }

    default: {
      return state;
    }
  }
}