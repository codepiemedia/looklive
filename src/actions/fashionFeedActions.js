import { FETCH_FASHION_FEED } from './actionTypes'

function dummyFeedItems() {
  // TODO: remove this when actual API is served
  return new Promise((resolve, reject) => {
    let feedItems = [];
    for(let i=0; i<20; i++) {
        feedItems.push({
            blogImgSrc: 'http://c.looklive.com/_cYRRwx4zu1O6t79AywxMYvUfyM=/0x0:400x600/400x610/a7b0195c-11a0-4def-b5f7-8a6f48929ff6',
            avatarImgSrc: 'http://c.looklive.com/hNDh7UBTRJ-2lFWs-ueL_RKU2Vg=/920x57:2551x1686/50x0/2b77d466-8cc4-495e-98e9-4fddced6269b',
            cardTitle: 'Top 10 Nike Gifts for Holiday Season',
            cardURL: '/',
            category: 'blog',
            dateTimeCreated: new Date('2017-05-06')
        })
    }
    return resolve(feedItems)
  })
}

export function fetchFashionFeed() {
  return {
    type: FETCH_FASHION_FEED,
    payload: dummyFeedItems()
  }
}