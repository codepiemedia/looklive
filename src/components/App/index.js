import Container from './App';
import UI from './AppUI';

const App = {
    Container: Container,
    UI: UI
};

export default App;
