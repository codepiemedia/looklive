import React, { Component } from 'react'
import { Provider } from 'react-redux'
import configureStore from '../configureStore'
import UserHomepageFeed from './UserHomepageFeed'

const store = configureStore()
export default class Root extends Component {

    render() {
        return (
            <Provider store={store}>
                <UserHomepageFeed />
            </Provider>
        )
    }
}
