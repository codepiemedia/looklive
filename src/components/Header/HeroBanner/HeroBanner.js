import { connect } from 'react-redux';

import HeroBannerUI from './HeroBannerUI';


const mapStateToProps = (state, ownProps) => {
    return {
    };
}

const mapDispatchToProps = dispatch => ({
    onLogout: () => {
    }
});

const HeroBannerContainer = connect(
    mapStateToProps, 
    mapDispatchToProps
)(HeroBannerUI);

export default HeroBannerContainer;