import React, { Component } from 'react';

class HeroBannerUI extends Component {

  render() {

    /* inline styles as banner image URL will be dynamic */
    const bannerImageStyles = {
      backgroundImage: `linear-gradient( rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0) ), url(${this.props.bannerImageURL})`
    }

    return (
      <article className="hero hero-intro" style={bannerImageStyles}>

        <div className="hero-inner hero-inner-intro">

          <a href={this.props.bannerLinkToURL} className="sponsor-hero" target="_blank">
            <h1 className="reset-margin-top">{this.props.bannerTitle}</h1>
            <h2 className="reset-margin-bottom">{this.props.bannerSubtitle}</h2>

          </a><a href={this.props.bannerLinkToURL} className="btn btn-cta sponsor-btn sponsor-hero">SHOP NOW</a>

        </div>
      </article>
    );
  }
}

export default HeroBannerUI;