import Container from './HeroBanner';
import UI from './HeroBannerUI';

const HeroBanner = {
    Container: Container,
    UI: UI
};

export default HeroBanner;
