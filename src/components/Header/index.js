import Container from './Header';
import UI from './HeaderUI';

const Header = {
    Container: Container,
    UI: UI
};

export default Header;
