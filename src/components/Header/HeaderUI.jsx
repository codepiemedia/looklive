import React, { Component } from 'react';
import GlobalNavigation from './GlobalNav'
import Logo from '../Assets/SVG/Logo.svg'
import SearchIcon from '../Assets/SVG/Search.svg'
import Cart from '../Assets/SVG/Cart.svg'

const GlobalNav = GlobalNavigation.UI;

class HeaderUI extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isMenuOpened: '',
            searchActive: ''
        }
    }

    toggleMenu() {
        this.setState({
            isMenuOpened: this.state.isMenuOpened === '' ? 'open' : '',
            searchActive: ''
        })
        
    }

    onSearchBtnClick() {
        this.setState({
            isMenuOpened: '',
            searchActive: this.state.searchActive === 'search-active' ? '' : 'search-active'
        })
    }

    render() {

        return (
            <header className="app-header navbar">
                <nav className={`navbar cf ${this.state.isMenuOpened} ${this.state.searchActive}`} >
                    <div className="navbar-menu-button navbar-menu-section" onClick={this.toggleMenu.bind(this)}>
                        <span className="navbar-burger"></span>
                    </div>
                    <div className="navbar-search-button navbar-menu-section" onClick={this.onSearchBtnClick.bind(this)}>

                    </div>
                    <a href="/" className="navbar-logo navbar-menu-section">
                        <img src={Logo} />
                    </a>

                    <form className="navbar-search-form navbar-menu-section" action="/search/" method="get">
                        <div className="navbar-search-field">
                            <span className="navbar-search-input-icon js-navbar-search-submit"><img className="svg-search" src={SearchIcon} /></span>
                            <input className="navbar-search-input" name="q" type="text" placeholder="Search Looklive.com"  />
                            <span className="navbar-search-close"></span>
                        </div>
                        <div className="navbar-search-results"></div>
                    </form>
                    <div className="flex-wrapper">
                        <div className="navbar-profile-menu-button navbar-menu-section">
                            <span className="navbar-burger"></span>
                            <div className="profile-menu">
                                <ul>
                                    <li><a href="/about">About</a></li>
                                    <li><a href="/about/careers">Careers</a></li>
                                    <li><a href="/about/press">Press</a></li>
                                    <li><a href="/about/contact">Contact</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>


                    <a className="navbar-wishcart navbar-menu-section" href="/account/login/?next=%2Faccount%2Fsettings%2Fupload-image%2F"> <img className="svg-wishcart" src={Cart} /> Upload Look</a>


                    <div className="navbar-user-area ">

                        <div className="user-unauthenticated">

                            <a href="/account/login/?next=%2F" className="btn">Login</a> &nbsp; or &nbsp; <a href="/account/register/" className="btn btn-cta">Join</a>
                        </div>

                    </div>

                    <GlobalNav />

                </nav>
            </header>
        )
    }
}

export default HeaderUI;
