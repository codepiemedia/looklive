import React, { Component } from 'react';

class GlobalNavUI extends Component {

  render() {
    return (
      <div className="global-nav">
        <ul className="navbar-menu">
          <li className="navbar-menu-item-selected navbar-menu-item"><a className="navbar-menu-link" href="/">Home</a></li>


          <li className="navbar-menu-item show-for-small-only">
            <a href="/account/register/" className="navbar-menu-link">Join</a>
          </li>
          <li className="navbar-menu-item show-for-small-only">
            <a href="/account/login/" className="navbar-menu-link">Login</a>
          </li>

          <li className="navbar-menu-item">
            <a className="navbar-menu-link" href="/discover/">Latest</a>
          </li>
          <li className="navbar-menu-item"><a className="navbar-menu-link" href="/products/">Discover</a></li>
          <li className="navbar-menu-item"><a className="navbar-menu-link" href="/people/">People</a></li>
          <li className="navbar-menu-item"><a className="navbar-menu-link" href="/blog/">Blog</a></li>
          <li className="navbar-menu-item"><a className="navbar-menu-link" href="/videos/">Videos</a></li>
          <li className="navbar-menu-item"><a className="navbar-menu-link" href="/brands/">Brands</a></li>
          <li className="navbar-menu-item show-for-small-only"><a className="navbar-menu-link" href="https://itunes.apple.com/app/id978632650">Download the iOS App</a></li>
        </ul>
      </div>
    );
  }
}

export default GlobalNavUI;