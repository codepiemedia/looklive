import Container from './Home';
import UI from './HomeUI';

const Home = {
    Container: Container,
    UI: UI
};

export default Home;
