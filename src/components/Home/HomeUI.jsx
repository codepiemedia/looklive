import React, { Component } from 'react';
import Header from './../Header';
import HeroBanner from './../Header/HeroBanner';
import FashionCard from './../Cards/FashionCard';

const HeaderComponent = Header.Container;
const Hero = HeroBanner.UI;

class HomeUI extends Component {

    componentDidMount() {
        this.props.fetchFashionFeed()
    }

    render() {

        const { feedItems } = this.props;

        return (
            <div>
                <HeaderComponent />
                <Hero 
                    bannerTitle={'SALUTE In STYLE.'} 
                    bannerSubtitle={'Don\'t Miss Out on the Exclusive Collection'} 
                    bannerLinkToURL={'/'} 
                    bannerImageURL={'/images/banner/STS-Homepage-Banner.png'}
                />

                {
                    feedItems.map((item, index) => {
                        return (
                            <FashionCard key={index}
                                blogImgSrc={ item.blogImgSrc }
                                cardURL={ item.cardURL }
                                cardTitle={ item.cardTitle }
                                category={ item.category }
                                dateTimeCreated={ item.dateTimeCreated }
                                avatarImgSrc={ item.avatarImgSrc }
                            />
                        )
                    })
                }

            </div>
        );
    }
}

export default HomeUI;
