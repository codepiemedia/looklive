import { connect } from 'react-redux';
import * as fashionFeedActions from './../../actions/fashionFeedActions'
import HomeUI from './HomeUI';


const mapStateToProps = (state, ownProps) => {
    return {
        feedItems: state.fashionFeed.feedItems
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchFashionFeed: () => dispatch(fashionFeedActions.fetchFashionFeed())
    };
};

const HomeContainer = connect(
    mapStateToProps, 
    mapDispatchToProps
)(HomeUI);

export default HomeContainer;