import React, { Component } from 'react'
import { Link } from 'react-router-dom'
export default class FashionCard extends Component {


  render() {
    return (
      <li className="feed-item feed-item-portrait">

        <Link to={this.props.cardURL}>
          <div className="link-blog-image-grid">
            <img className="link-blog-image-self" src={this.props.blogImgSrc} />
          </div>
        </Link>

        <div className="feed-item-content">
          <div className="media story-media avatar-media">
            <div className="media-left">
              <img src={this.props.avatarImgSrc} />
            </div>
            <div className="media-body">
              <span className="feed-item-title">{this.props.cardTitle} </span>
              <span className="feed-item-descr">{this.props.category}</span>
              <span className="feed-item-created"><i className="icon icon-timeago"></i> {this.props.dateTimeCreated.toISOString()}</span>
            </div>
          </div>
        </div>
      </li>
    )
  }
}