'use strict'

let gulp = require('gulp'),
    sass = require('gulp-sass'),
    postcss = require('gulp-postcss'),
    cssnano = require('gulp-cssnano'),
    autoprefixer = require('autoprefixer'),
    cleanCSS = require('gulp-clean-css'),
    log = console.log;

const plugins = [
    autoprefixer()
];

const cssFilePath = './src/styles/styles.scss';
const cssOutputPath = './public/css/';

gulp.task('sass', function () {
    log('**** generating css **** ');
    log('');
 
    gulp.src(cssFilePath)
        .pipe(sass({ style: 'expanded' }))
        .pipe(postcss(plugins))
        .pipe(cssnano())
        .pipe(cleanCSS({ compatibility: 'ie10' }))
        .pipe(gulp.dest(cssOutputPath));

    log('');
    log('finished generating css file');

});


// Watching static files
gulp.task('watch', () => {
    gulp.watch('./src/styles/**/*.scss', ['sass']);
});

gulp.task('default', ['sass']);
